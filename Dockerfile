ARG BASE=alpine:3.12
FROM ${BASE}

RUN echo "@community http://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories
RUN apk --no-cache add py3-pandas@community py3-pip git

RUN pip3 install inotify

RUN mkdir /build && cd /build && \
    git clone --depth 1 https://gitlab.com/appf-anu/pytelegraf.git . && \
    python3 setup.py install
ENV PYTHONUNBUFFERRED=1

WORKDIR /app
COPY run.py .
ENTRYPOINT ["python3", "run.py"]
