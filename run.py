#!/usr/bin/env python3
import pandas
from pandas.errors import ParserError

import io
import glob
import time
import inotify.adapters
import inotify.constants
import os

from telegraf.client import TelegrafClient

import datetime


telegraf_host = os.environ.get("TELEGRAF_HOST", "telegraf")
telegraf_port = int(os.environ.get("TELEGRAF_PORT", 8092))
watch_dir = os.environ.get("WATCH_DIR", "/data")
station = os.environ.get("STATION", "STN_NAME")


def dateparse(x)->datetime.datetime:
    """
    parses csis _kinda_ iso datetime

    Args:
        x (str): datetime string

    Returns:
        TYPE: Description
    """
    return datetime.datetime.strptime(x, '%Y-%m-%d %H:%M:%S')


def read_csi_file(fn) -> pandas.DataFrame:
    """
    reads a campbell scientific TOA5 file (.dat) to a pandas dataframe and
    returns it

    incomplete data files will be incompletely read.

    Args:
        fn (str): file name of the .dat file to be read

    Returns:
        pandas.DataFrame: DataFrame of the data in the .dat file

    """
    with open(fn) as f:
        dat_buffer = io.StringIO(f.read())

    try:
        return pandas.read_csv(dat_buffer,
                               skiprows=[0, 2, 3],
                               index_col=0,
                               parse_dates=True,
                               infer_datetime_format=True,
                               na_values=['NAN', "-INF", 7999, -7999],
                               low_memory=False)

    except ParserError:
        tags = dict()
        infodf = pandas.read_csv(fn, nrows=1, header=None)
        names = ("filetype", "station", "model", "serial", "os", "program", "signature", "table")
        for i in infodf:
            if i >= len(names):
                continue
            tags[names[i]] = str(infodf[i][0]).replace(" ", "_").replace("\'", "").replace("\"", "").replace(",", "").replace(":", "_")
        if tags.get('station') not in (station, None, "STN_NAME"):
            tags['station_file'] = tags.get("station")
        if tags.get('station') in (None, "STN_NAME"):
            tags['station'] = station

        client = TelegrafClient(host=telegraf_host, port=telegraf_port)
        client.metric("cr1000x_errors",
                      {"error": "initial parse error"},
                      tags=tags)

        dat_buffer.seek(0)
        lines = dat_buffer.readlines()
        lines = lines[:-1]  # cut the last line because they upload incomplete data
        ndat_buffer = io.StringIO("".join(lines))
        try:
            return pandas.read_csv(ndat_buffer,
                                   skiprows=[0, 2, 3],
                                   index_col=0,
                                   parse_dates=True,
                                   infer_datetime_format=True,
                                   na_values=['NAN', "-INF", 7999, -7999],
                                   low_memory=False)
        except ParserError:
            raise


def output_telegraf_data(fn):
    """
    reads a campbell scientific TOA5 file (.dat) and outputs a series of telegraf
    metrics for that file

    Args:
        fn (str): filename of the .dat file to be read
    """
    print(f"processing {fn}")

    tags = dict()
    try:
        # example header:
        # "TOA5","STN_NAME","CR1000X","15027","CR1000X.Std.04.02","CPU:CR1000X program 30Oct.cr1x","3623","DT_30_Min"
        infodf = pandas.read_csv(fn, nrows=1, header=None)
        names = ("filetype", "station", "model", "serial", "os", "program", "signature", "table")
        for i in infodf:
            if i >= len(names):
                continue
            tags[names[i]] = str(infodf[i][0]).replace(" ", "_").replace("\'", "").replace("\"", "").replace(",", "").replace(":", "_")
        if tags.get('station') not in (station, None, "STN_NAME"):
            tags['station_file'] = tags.get("station")
        if tags.get('station') in (None, "STN_NAME"):
            tags['station'] = station
    except Exception as e:
        print(f"error decoding {fn}: {e}")
        return
    try:
        written = 0
        df = read_csi_file(fn)
        client = TelegrafClient(host=telegraf_host, port=telegraf_port)
        for pd_ts, row in df.iterrows():
            # wtf sometimes we get null timestamps, skip them
            if pandas.isnull(pd_ts):
                continue
            # pytelegraf doesnt support pandas timestamps
            pyts = pd_ts.to_pydatetime()
            epoch_ns = int(pyts.timestamp() * 1e9)  # to NS

            metric_data = dict(fn=fn)
            for k, v in row.items():
                # skip nan because influxdb has no concept of nan
                if pandas.isnull(v):
                    continue

                try:
                    # dont even try to parse timestamp values
                    ts = dateparse(v)
                    v = ts.timestamp() * 1e9

                    continue
                except:
                    pass

                # set timestamps to int nanos
                if type(v) == pandas.Timestamp:
                    try:

                        metric_data[k] = int(v.to_pydatetime().timestamp() * 1e9)
                    except Exception as e:
                        print(e)
                    continue
                if type(v) == int and not k == "RECORD":
                    v = float(v)
                if k == "RECORD":
                    v = int(v)
                metric_data[k] = v
            if len(metric_data):
                client.metric("cr1000x", metric_data, tags=tags, timestamp=epoch_ns)
                written += 1
            else:
                client.metric("cr1000x_errors", {"error": "no data from row"}, tags=tags, timestamp=epoch_ns)
                pass
    except Exception as e:
        client.metric("cr1000x_errors", {"exc": e, "fn": fn})
    if os.environ.get("DELETE", "true").lower() not in ("false", "0", "no", ""):
        os.remove(fn)


def run_batch():
    files = glob.glob(os.path.join(watch_dir, "*.dat"))
    # time.sleep(120)
    for fn in files:
        output_telegraf_data(fn)


if __name__ == "__main__":

    while True:
        print("startup")
        run_batch()
        inot = inotify.adapters.InotifyTree(watch_dir)
        # i = inotify.adapters.Inotify()
        # i.add_watch(watch_dir, mask=inotify.constants.IN_CLOSE_WRITE | inotify.constants.IN_MOVED_TO)
        for _ in range(600):
            for ev in inot.event_gen(yield_nones=False, timeout_s=1):
                _, type_names, path, filename = ev
                in_fullpath = os.path.join(path, filename)
                if 'IN_ISDIR' in type_names or ('IN_CLOSE_WRITE' not in type_names and 'IN_MOVED_TO' not in type_names):
                    # print(f"ignored {','.join(type_names)}")
                    continue
                if os.path.splitext(in_fullpath)[-1] != '.dat':
                    print(f'path "{in_fullpath}" doesnt end with .dat')
                    continue
                output_telegraf_data(in_fullpath)
        # clean up inotify watcher
        del inot
